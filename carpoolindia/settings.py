# Django settings for carpoolindia project.
import dj_database_url
import os

SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

# Static asset configuration
BASE_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), '..')
PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))


DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    ('Kumar Varadarajulu', 'carpool.inindia@gmail.com'),
)

MANAGERS = ADMINS

if dj_database_url.config():
    DATABASES = {
        'default': dj_database_url.config(),
        'OPTIONS': {
            'timezone': 'UTC',
        }
    }
else:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
            'NAME': 'carpoolindia_db',                      # Or path to database file if using sqlite3.
            # The following settings are not used with sqlite3:
            'USER': 'carpool',
            'PASSWORD': 'carpool',
            'HOST': 'localhost',                      # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
            'PORT': '5432',                      # Set to empty string for default.
        },
        'OPTIONS': {
            'timezone': 'UTC',
        }
    }

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
ALLOWED_HOSTS = ['localhost', '.carpoolindia.in', 'carpoolindia.herokuapp.com']

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'Asia/Kolkata'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/var/www/example.com/media/"
MEDIA_ROOT = ''

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://example.com/media/", "http://media.example.com/"
MEDIA_URL = ''

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/var/www/example.com/static/"
STATIC_ROOT = 'staticfiles'

# URL prefix for static files.
# Example: "http://example.com/static/", "http://static.example.com/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(PROJECT_ROOT, 'static'),
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = '301qkd9$x1_974!nb9t9jhri(oz4pxj4$h@t$#scpt2v6glli2'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'carpoolindia.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'carpoolindia.wsgi.application'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(BASE_DIR, 'carpoolindia/templates'),
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'carpoolindia',
    'carpoolapp',
    # Uncomment the next line to enable the admin:
    'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'mail.carpoolindia@gmail.com'
EMAIL_HOST_PASSWORD = '143sachin'
EMAIL_PORT = 587
ACTIVATION_LINK_HOST = "http:\\\\carpoolindia.herokuapp.com\\mapp\\activate\\"


# Thumbnail
THUMBNAIL_DEBUG = True
THUMBNAIL_UPLOAD_TO_DIR = 'uploaded_files/'

try:
    from local_settings import *
except Exception as e:
    print e.message

"""AWS_ACCESS_KEY_ID = os.environ['AWS_ACCESS_KEY_ID']
AWS_SECRET_ACCESS_KEY = os.environ['AWS_SECRET_ACCESS_KEY']
AWS_STORAGE_BUCKET_NAME = os.environ['AWS_STORAGE_BUCKET_NAME']
"""

AWS_STORAGE_BUCKET_NAME = 'dealfreaks'
AWS_QUERY_STRING_AUTH = False
AWS_PRELOAD_METADATA = True

AWS_HEADERS = {
    'Cache-Control':'max-age=86400',
}

AWS_IS_GZIPPED = True
AWS_PRELOAD_METADATA = True
COMPRESS_OFFLINE = False
#if not DEBUG:
DEFAULT_FILE_STORAGE = 'deals.s3utils.MediaS3BotoStorage'
#AWS: STATICFILES_STORAGE = 'deals.s3utils.StaticS3BotoStorage'
#AWS: COMPRESS_STORAGE = 'deals.s3utils.CompressedS3BotoStorage'
COMPRESS_STORAGE = 'compressor.storage.GzipCompressorFileStorage'

COMPRESS_OUTPUT_DIR = 'compressed/'

DEFAULT_FILE_STORAGE = 'deals.s3utils.MediaS3BotoStorage'
S3_URL = 'https://%s.s3.amazonaws.com' % AWS_STORAGE_BUCKET_NAME
STATIC_DIRECTORY = '/static/'
MEDIA_DIRECTORY = '/media/'
#AWS: STATIC_URL = S3_URL + STATIC_DIRECTORY
MEDIA_URL = S3_URL + MEDIA_DIRECTORY
COMPRESS_URL = STATIC_URL

DEFAULT_AVATAR_NAME = 'default-avatar.jpg'
DEFAULT_AVATAR = MEDIA_URL + THUMBNAIL_UPLOAD_TO_DIR + DEFAULT_AVATAR_NAME
DEFAULT_AVATAR_THUMBNAIL = 'https://s3-ap-southeast-1.amazonaws.com/dealfreaks/media/cache/c3/a3/c3a3344838ae9d234f26442f8ad9ac2b.jpg'
DEFAULT_AVATAR_THUMBNAIL_SMALL = 'https://dealfreaks.s3.amazonaws.com/media/cache/a2/15/a21520ec306d54bc149334b236130a3a.jpg'
DEFAULT_AVATAR_THUMBNAIL_XSMALL = 'https://dealfreaks.s3.amazonaws.com/media/cache/69/3c/693c4715cca45cd9887a552fcc124b2e.jpg'