from django.http import HttpResponse
import json, hashlib
from carpoolapp.models import UserAuthKey
from django.contrib.auth.models import User
from utils import init_json, getUser
from django.core.exceptions import ObjectDoesNotExist


def logedin(function):
    def wrap(request, *args, **kwargs):
        if request.GET:
            devkey = request.GET.get('sky', '')
            devid = request.GET.get('did', '')
            usr = request.GET.get('user', '')
        elif request.POST or request.is_ajax():
            devkey = request.POST.get('sky', '')
            devid = request.POST.get('did', '')
            usr = request.POST.get('user', '')
        encdid = hashlib.sha1( devid[0] + devid[1] + devid[2] + devid ).hexdigest()
        authkey = hashlib.sha1( devkey + encdid ).hexdigest()
        try:
            user = User.objects.get(username=usr)
            auth = UserAuthKey.objects.get(authkey=authkey, user=user)
            return function(request, *args, **kwargs)
        except ObjectDoesNotExist:
            # Since the authertication failed send command to delete all tables. Seems to be some problem
            # Trigger is DEL an parameter is ALL for delete all tables
            json_response = init_json()
            json_response['status'] = 'f'
            json_response['errorcode'] = 'LGNF'
            json_response['message'] = 'Login failed'
            json_response['trigger'] = [('DEL', 'ALL')]
        return HttpResponse(json.dumps(json_response), content_type='application/json')
    return wrap