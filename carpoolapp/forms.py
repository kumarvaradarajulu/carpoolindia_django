from carpoolapp.models import *
from django.contrib.auth.models import User
from carpoolapp.models import UserAuthKey
from django.contrib.auth.forms import UserCreationForm, PasswordChangeForm
from django import forms
import re
from django.core.exceptions import ObjectDoesNotExist
from utils import *
class MyRegistrationForm(UserCreationForm):
    email = forms.EmailField(required=True)

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2', 'first_name', 'last_name')

    def save(self, commit=True):
        user = super(MyRegistrationForm, self).save(commit=False)
        user.email = self.cleaned_data['email']
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        if commit:
            user.save()

        return user

    def clean_email(self):
        data = self.cleaned_data['email']
        if User.objects.filter(email=data).exists():
            raise forms.ValidationError("Email Id already registered, please login")
        return data

    def clean_username(self):
        data = self.cleaned_data['username'].lower()
        if not re.match(r'^[a-zA-Z0-9]+$', data):
            raise forms.ValidationError("Username should contain alphabets and numbers only")
        elif User.objects.filter(username=data).exists():
            raise forms.ValidationError("Username %s is already used, please choose another name" % data)

        return data

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")

        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'])

        if len(password1) < 6:
            raise forms.ValidationError('Password too short, Must be minimum 6 chars long.')
        return password2


class UserProfileForm(forms.Form):
    type = forms.CharField(required='True', max_length=1)
    startlocation = forms.CharField(required='True', max_length=200)
    endlocation = forms.CharField(required='False', max_length=200)
    start_work_time = forms.TimeField(required=False)
    end_work_time = forms.TimeField(required=False)
    gender = forms.CharField(required=True, max_length=1)
    preferred_gender = forms.CharField(required=False, max_length=1)
    phone = forms.CharField(max_length=10, required=False)

    class Meta:
        model = Profile
        fields = ('user','type', 'start_work_time', 'end_work_time', 'gender', 'preferred_gender', 'phone')


    def save(self, commit=True):
        user = User.objects.get(username = self.data['user'])
        try:
            profile = Profile.objects.get(user=user)
        except ObjectDoesNotExist:
            profile = Profile()
        profile.user = user
        profile.type = self.cleaned_data['type']
        profile.startlocation = self.cleaned_data['startlocation']
        profile.endlocation = self.cleaned_data['endlocation']
        profile.start_work_time = self.cleaned_data['start_work_time']
        profile.end_work_time = self.cleaned_data['end_work_time']
        profile.gender = self.cleaned_data['gender']
        profile.phone = self.cleaned_data['phone'] if self.cleaned_data['phone'] else ''
        profile.preferred_gender = self.cleaned_data['preferred_gender']
        userauthkey = UserAuthKey.objects.get(user=user)
        userauthkey.date_time = now()

        if commit:
            userauthkey.save()
            profile.save()


class UserVehicleForm(forms.Form):
    make = forms.CharField(required=True, max_length=100)
    model = forms.CharField(required=True, max_length=200)
    color = forms.CharField(required=False, max_length=30)
    year = forms.CharField(required=False, max_length=7)
    fueleff = forms.CharField(required=False, max_length=3)
    fuel = forms.CharField(max_length=1, required=True)
    pref_gender = forms.CharField(required=False, max_length=1)
    totalmembers = forms.CharField(required=False, max_length=2)
    occupied = forms.CharField(max_length=2, required=True)
    freeseats = forms.CharField(required=False, max_length=2)

    class Meta:
        model = UserVehicle
        fields = ('user', 'make', 'model', 'color', 'year', 'fueleff', 'fuel', 'pref_gender', 'totalmembers','occupied','freeseats')

    def save(self, commit=True):
        user = User.objects.get(username = self.data['user'])
        try:
            vehicle = UserVehicle.objects.get(user = user)
        except ObjectDoesNotExist:
            vehicle = UserVehicle()
        vehicle.user = user
        vehicle.make = self.cleaned_data['make']
        vehicle.model = self.cleaned_data['model']
        vehicle.color = self.cleaned_data['color']
        vehicle.year = self.cleaned_data['year']
        vehicle.fueleff = self.cleaned_data['fueleff']
        vehicle.fuel = self.cleaned_data['fuel']
        userauthkey = UserAuthKey.objects.get(user=user)
        userauthkey.date_time = now()

        if commit:
            userauthkey.save()
            vehicle.save()

class PoolMemberForm(forms.Form):

    startlocation = forms.CharField(required=True, max_length=200)
    startpos = forms.CharField(required=True, max_length=200)
    endlocation = forms.CharField(required=False, max_length=200)
    endpos = forms.CharField(required=False, max_length=200)
    start_date = forms.DateTimeField(required=False)
    end_date = forms.DateTimeField(required=False)

    class Meta:
        model = PoolMembers
        fields = ('pool','user','startlocation', 'startpos', 'endlocation', 'endpos','start_date', 'end_date')

    def save(self, commit=True):
        poolmem = PoolMembers()

        user = User.objects.get(username = self.data['user'])
        poolmem.user = user
        poolmem.startlocation = self.cleaned_data['startlocation']
        poolmem.startpos = self.cleaned_data['startpos']
        poolmem.endlocation = self.cleaned_data['endlocation']
        poolmem.endpos = self.cleaned_data['endpos']
        poolmem.start_date = self.cleaned_data['start_date']
        poolmem.end_date = self.cleaned_data['end_date']
        userauthkey = UserAuthKey.objects.get(user=user)
        userauthkey.date_time = now()

        if commit:
            userauthkey.save()
            poolmem.save()

class ChangePasswordForm(forms.Form):
    old_password = forms.CharField(widget=forms.PasswordInput)
    new_password1 = forms.CharField(widget=forms.PasswordInput)
    new_password2 = forms.CharField(widget=forms.PasswordInput)
    
    class Meta:
        model = User
        fields = ('old_password','new_password1','new_password2')

    def clean_old_password(self):
        """
        Validates that the old_password field is correct.
        """
        self.user = User.objects.get(username = self.data['user'])
        old_password = self.cleaned_data["old_password"]
        if not self.user.check_password(old_password):
            raise forms.ValidationError("Your old password was entered incorrectly.Please enter it again.")
        return old_password

    def clean_new_password2(self):
        new_password1 = self.cleaned_data.get("new_password1")
        new_password2 = self.cleaned_data.get("new_password2")
        if new_password1 and new_password2 and new_password1 != new_password2:
            raise forms.ValidationError("The two password fields didn't match")

        if len(new_password1) < 6:
            raise forms.ValidationError('Password too short, Must be minimum 6 chars long.')
        return new_password2
            
    def save(self, commit=True):
        self.user.set_password(self.cleaned_data['new_password1'])
        authkey = UserAuthKey.objects.get(user = self.user)
        authkey.passreset = False
        if commit:
            self.user.save()
            authkey.save()
        return self.user
