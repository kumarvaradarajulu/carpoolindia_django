import requests
import json

AUTO_COMPLETE_API_URL='https://maps.googleapis.com:443/maps/api/place/autocomplete/json?input='
GEOCODE_API_URL='https://maps.googleapis.com/maps/api/geocode/json?address='
DISTANCE_API_URL='http://maps.googleapis.com/maps/api/distancematrix/json?'
API_KEY='AIzaSyAgjwCxmJ0ohDPokoV7f160GRTnbYUvqxU'

class PlaceAutoComplete:
    def __init__(self):
        pass
    
    def autocompletes(self, searchkey, radius='500', location=''):
        lst_loc = list()
        status = ''
        url = AUTO_COMPLETE_API_URL + searchkey
        if location:
           url += '&location=' + location
        if radius:
            url += '&radius=' + radius
        url += '&sensor=false&key=' + API_KEY
        r = requests.get(url)
        data = json.loads(r.text)
        status = data['status']
        if status == 'OK':
            for item in data['predictions']:
                lst_loc.append(item['description'])
        return (status, lst_loc)

class GeoCode:
    def __init__(self):
        pass

    def geocode(self,place):
        location = {}
        status = ''
        url = GEOCODE_API_URL + place
        url +='&sensor=false&key=' + API_KEY
        r = requests.get(url)
        data = json.loads(r.text)
        status = data['status']
        if status == 'OK':
            result = data['results'] 
            if result:
                location = result[0]['geometry']['location']
                # import pprint
                # pprint.pprint(result)
        return (status,location)

    def driving_distence(self,from_loc,to_loc):
        status = ''
        km=''
        meter=0
        url=DISTANCE_API_URL + 'origins=' + from_loc + '&destinations=' + to_loc + '&mode=driving&language=en-EN&sensor=false'
        r = requests.get(url)
        data = json.loads(r.text)
        status = data['status']
        if status == 'OK':
            if data['rows'][0]['elements'][0]['status'] == 'OK':
                km = data['rows'][0]['elements'][0]['distance']['text']
                meter = data['rows'][0]['elements'][0]['distance']['value']
        return (status,km,meter)

if __name__ == "__main__":
    pac = PlaceAutoComplete()
    geo = GeoCode()
    print pac.autocompletes('bellandur')
    print geo.geocode('Marathalli Bridge')
    print geo.driving_distence('12.935642,77.688917','11.243664,75.775454')
