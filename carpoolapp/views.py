from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.contrib import auth
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.views.decorators.csrf import csrf_exempt

import hashlib
import json
import string, random
from carpoolapp.decorators import logedin
from utils import init_json, getUser, getProfile, getVehicle, getPool
from carpoolapp.forms import *
from carpoolapp.models import * 
from django.utils.dateparse import parse_datetime
from django.core.mail import EmailMessage
from django.conf import settings
from google import PlaceAutoComplete

@csrf_exempt
def register(request):
    """
        Error Codes:
            REG1 = No device ID provided
            REG2 = Form is invalid

    """
    json_response = init_json()
    if request.POST or request.is_ajax():
        registerform = MyRegistrationForm(request.POST)
        if registerform.is_valid():
            data = registerform.cleaned_data
            registerform.save()
            user = auth.authenticate(username=data['username'], password = data['password1'])
            auth.login(request, user)
            devid = request.POST.get('did', '')
            if devid:
                (devkey, json_response['data']['date_time']) = createAuthKey(devid, user)
                json_response['data']['key'] = devkey
                json_response['data']['did'] = devid
                json_response['data']['user'] = data['username']
            else:
                json_response['status'] = 'f'
                json_response['message'] = 'Error Registering, please report problem to us'
                json_response['errorcode'] = 'REG1'
        else:
            json_response['errorcode'] = 'REG2'
            json_response['status'] = 'f'
            err = dict()
            json_response['message'] = 'Form validation failed'
            for key, mes in registerform.errors.iteritems():
                if key == 'username':
                    key = 'rusername'
                err[key]=mes
                print key,mes
            json_response['err_data'] = err

    print json.dumps(json_response)
    return HttpResponse(json.dumps(json_response), content_type='application/json')


def createAuthKey(devid, user):
    authkey = UserAuthKey()
    devkey = hashlib.sha1( devid[2] + devid[1] + devid[0] + devid ).hexdigest()
    authkey.devid = hashlib.sha1( devid[0] + devid[1] + devid[2] + devid ).hexdigest()
    authkey.authkey = hashlib.sha1( devkey + authkey.devid).hexdigest()
    authkey.devkey = devkey
    authkey.activated = True
    authkey.user = user
    date_time = now()
    authkey.date_time = date_time
    authkey.save()

    useractv = UserActivtion()
    useractv.user = user
    useractv.authkey = authkey
    useractv.acvtkey = hashlib.sha1( user.username + authkey.authkey).hexdigest()
    useractv.date_time = now()
    useractv.save()
    sendActivationLink(user.email, useractv.acvtkey)
    return (devkey, str(date_time))

def sendActivationLink(email, acvtkey):
    subject = 'Carpoolindia User activation'
    body = 'Please click visit the link to activate you account in carpoolindia \n'
    body += settings.ACTIVATION_LINK_HOST + acvtkey
    emailsend = EmailMessage(subject, body, settings.EMAIL_HOST_USER, (email,))
    emailsend.send()

@csrf_exempt
def activate(request,actid=''):
    try:
        userAct = UserActivtion.objects.get(acvtkey=actid)
    except ObjectDoesNotExist:
        msg='Invalid Activation link'
    else:
        if userAct:
            authkey = userAct.authkey
            authkey.activated = True
            authkey.save()
            userAct.delete()
            msg="Your account with user name <b>"+authkey.user.username+"</b> has been activated."
        else:
            msg='Activation link expired'
    return HttpResponse(msg)

@csrf_exempt
def login(request):
    """
        Error Codes:
            LOG1 = Username pasword is worng
            LOG2 = No devid or password

    """
    json_response = init_json()
    if request.POST or request.is_ajax():
        username = getUser(request.POST.get('username', ''))
        if not username:
            json_response['status'] = 'f'
            json_response['message'] = 'User does not exisits'
            json_response['errorcode'] = 'ACT1'
            return HttpResponse(json.dumps(json_response), content_type='application/json')
        else:
            try:
                old_authkey = UserAuthKey.objects.get(user=username)
            except ObjectDoesNotExist:
                pass
            else:
                if not old_authkey.activated:
                    json_response['status'] = 'f'
                    json_response['message'] = 'You account is not activated'
                    json_response['errorcode'] = 'ACT1'
                    return HttpResponse(json.dumps(json_response), content_type='application/json')
        user = auth.authenticate(username=request.POST.get('username', ''), password = request.POST.get('password', ''))
        if user:
            devid = request.POST.get('did', '')
            if devid:
                encdid = hashlib.sha1( devid[0] + devid[1] + devid[2] + devid ).hexdigest()
                auth.login(request, user)
                passwordreset = False
                try:
                    authkey = UserAuthKey.objects.get(devid=encdid, user=user)
                except ObjectDoesNotExist:
                    try:
                        old_authkey = UserAuthKey.objects.get(user=user)
                        passwordreset = old_authkey.passreset
                    except ObjectDoesNotExist:
                        pass
                    (devkey, json_response['data']['date_time']) = createAuthKey(devid, user)
                    json_response['data']['did'] = devid
                    json_response['data']['key'] = devkey
                    json_response['data']['passreset'] = passwordreset
                    json_response['data']['user'] = user.username
                    json_response['trigger'] = getSyncData(user, False)
                else:
                    if authkey:
                        if not authkey.activated:
                            json_response['status'] = 'f'
                            json_response['message'] = 'You account is not activated'
                            json_response['errorcode'] = 'ACT1'
                        else:
                            json_response['data']['did'] = devid
                            json_response['data']['key'] = authkey.devkey
                            json_response['data']['user'] = user.username
                            json_response['data']['date_time'] = str(authkey.date_time)
                            json_response['data']['passreset'] = authkey.passreset
                            json_response['trigger'] = getSyncData(user, False)
                    else:
                        json_response['status'] = 'f'
                        json_response['message'] = 'Error logging in, please report problem to us'
                        json_response['errorcode'] = 'LOG2'
            else:
                json_response['status'] = 'f'
                json_response['message'] = 'Error logging in, please report problem to us'
                json_response['errorcode'] = 'LOG2'
        else:
            json_response['status'] = 'f'
            json_response['errorcode'] = 'LOG1'
            json_response['message'] = 'Username / Password does not match'
    print json.dumps(json_response)
    return HttpResponse(json.dumps(json_response), content_type='application/json')

@csrf_exempt
@logedin
def sync(request):
    """
        Error Codes:
            REG1 = No device ID provided
            REG2 = Form is invalid

    """
    json_response = init_json()

    if request.POST or request.is_ajax():
        devkey = request.POST.get('sky', '')
        devid = request.POST.get('did', '')
        date_time = parse_datetime(request.POST.get('date', ''))
        encdid = hashlib.sha1( devid[0] + devid[1] + devid[2] + devid ).hexdigest()
        authkey = hashlib.sha1( devkey + encdid).hexdigest()
        usr = request.POST.get('user', '')
        first = request.GET.get('first', False)
        user = getUser(usr)
        try:
            auth = UserAuthKey.objects.get(authkey=authkey, user=user)
            if auth.date_time > date_time:
                json_response['trigger'] = getSyncData(user, first)
        except ObjectDoesNotExist:
            json_response['status'] = 'f'
            json_response['message'] = 'Sync Error'

 # POOL id owner startlocation, startpos, endlocation, endpos, journey_start_time, journey_end_time, preferred_gender,
 # totalmembers, occupied, freeseats, poolvehic, date_time)');
# PROFILE (id, user, type, type_change_date, start_work_time, end_work_time, gender, pool, date_time, phone)');
# BUZZ (id, buzzed_by, pool, buzz_msg, buzz_importance, date_time)');
#AUTH (id, user, pool, sky, did, date_time)');
# POOLCALENDAR (id, pool, month, daysoff, dayson , date_time)');
#POOLMEMBERS (id, pool, user, startlocation, startpos, endlocation, endpos, distancefromowner, date_time)

    return HttpResponse(json.dumps(json_response), content_type='application/json')


def getSyncData(user, first):
    data = list()
    try:
        profile = Profile.objects.get(user=user)
        data.append({'TYPE':'UPD', 'CODE':'PRFL', 'DATA':[profile.serialized()]})
    except ObjectDoesNotExist:
        pass
    try:
        pool = Pool.objects.get(owner=user)
        data.append({'TYPE':'UPD', 'CODE':'POL', 'DATA':[pool.serialized()]})
        try:
            poolcalendar = PoolCalendar.objects.get(pool=pool)
            data.append({'TYPE':'UPD', 'CODE':'PCAL', 'DATA':[poolcalendar.serialized()]})
        except ObjectDoesNotExist:
            pass
        try:
            poolmembers = PoolMembers.objects.get(user=user)
            data.append({'TYPE':'UPD'
                         , 'CODE':'PMEM',
                         'DATA':[seri.serialized() for seri in poolmembers]})
            otherpools = PoolMembers.objects.filter(pool=poolmembers.pool)
            if otherpools:
                data.append({'TYPE':'UPD', 'CODE':'PMEM', 'DATA':[seri.serialized() for seri in otherpools]})
            poolmemberscal = PoolMembersCalendar.objects.filter(pool=poolmembers.pool)
            if poolmemberscal:
                data.append({'TYPE':'UPD', 'CODE':'PMCAL', 'DATA':[poolmemberscal.serialized()]})
        except ObjectDoesNotExist:
            pass
    except ObjectDoesNotExist:
        pass

    buzz = Buzz.objects.filter(buzzed_to=user)
    if buzz:
        data.append({'TYPE':'UPD', 'CODE':'BZZ', 'DATA':[seri.serialized() for seri in buzz]})
    return data


@csrf_exempt
def update_profile(request):
    """
        Error Codes:
            PRO1 = Error Updating profile
            PRO2 = Form is invalid

    """
    json_response = init_json()
    if request.POST or request.is_ajax():
        profileform = UserProfileForm(request.POST)
        if profileform.is_valid():
            data = profileform.cleaned_data
            profileform.save()
            if data:
                json_response['trigger'] = getSyncData(getUser(request.POST.get('user', '')), False)
            else:
                json_response['status'] = 'f'
                json_response['message'] = 'Error Updating profile, please report problem to us'
                json_response['errorcode'] = 'PRO1'
        else:
            json_response['errorcode'] = 'PRO2'
            json_response['status'] = 'f'
            err = dict()
            json_response['message'] = 'Form validation failed'
            for key, mes in profileform.errors.iteritems():
                err[key]=mes
                print key,mes
            json_response['err_data'] = err

    return HttpResponse(json.dumps(json_response), content_type='application/json')


@csrf_exempt
def vehicle_details(request):
    """
        Error Codes:
            VEH1 = Error Updating profile
            VEH2 = Form is invalid

    """
    json_response = init_json()
    if request.POST or request.is_ajax():
        vehicleform = UserVehicleForm(request.POST)
        if vehicleform.is_valid():
            data = vehicleform.cleaned_data
            vehicleform.save()
            createPool(vehicleform.data)
            if data:
                json_response['trigger'] = getSyncData(getUser(request.POST.get('user', '')), False)
            else:
                json_response['status'] = 'f'
                json_response['message'] = 'Error Updating profile, please report problem to us'
                json_response['errorcode'] = 'VEH1'
        else:
            json_response['errorcode'] = 'VEH2'
            json_response['status'] = 'f'
            err = dict()
            json_response['message'] = 'Form validation failed'
            for key, mes in vehicleform.errors.iteritems():
                err[key]=mes
                print key,mes
            json_response['err_data'] = err
            
    return HttpResponse(json.dumps(json_response), content_type='application/json')


@csrf_exempt
def poolmember_details(request):
    """
        Error Codes:
            PLM1 = Error Updating profile
            PLM2 = Form is invalid

    """
    json_response = init_json()
    if request.POST or request.is_ajax():
        poolmemform = PoolMemberForm(request.POST)
        if poolmemform.is_valid():
            data = poolmemform.cleaned_data
            poolmemform.save()
            if data:
                json_response['data'] = poolmemform.data
            else:
                json_response['status'] = 'f'
                json_response['message'] = 'Error Pool Member, please report problem to us'
                json_response['errorcode'] = 'PLM1'
        else:
            json_response['errorcode'] = 'PLM2'
            json_response['status'] = 'f'
            err = dict()
            json_response['message'] = 'Form validation failed'
            for key, mes in poolmemform.errors.iteritems():
                err[key]=mes
                print key,mes
            json_response['err_data'] = err
            
    return HttpResponse(json.dumps(json_response), content_type='application/json')

def test(request):
    args = {}
    return render_to_response('carpoolapp/mobile_app.html', args)

def createPool(data):
    user = getUser(data['user'])
    try:
        pool = Pool.objects.get(owner = user)
    except ObjectDoesNotExist:
        pool = Pool()
    profile = getProfile(user)
    vehicle = getVehicle(user)
    pool.owner = user
    pool.poolvehic = vehicle
    pool.startlocation = profile.startlocation
    pool.startpos = profile.startpos
    pool.endlocation = profile.endlocation
    pool.endpos = profile.endpos
    #pool.distance = find_distance()
    pool.journey_start_time = profile.start_work_time
    pool.journey_end_time = profile.end_work_time
    pool.preferred_gender = data['pref_gender']
    pool.totalmembers = data['totalmembers']
    pool.occupied = data['occupied']
    pool.freeseats = data['freeseats']
    pool.save()
    profile.pool = pool
    profile.save()

@csrf_exempt
def change_password(request):
    """
        Error Codes:
            CHP1 = Error Reset Password
            CHP2 = Form is invalid

    """
    json_response = init_json()
    if request.POST or request.is_ajax():
        chpassform = ChangePasswordForm(request.POST)
        if chpassform.is_valid():
            data = chpassform.cleaned_data
            chpassform.save()
            if data:
                json_response['data'] = chpassform.data
            else:
                json_response['status'] = 'f'
                json_response['message'] = 'Error Reset Password'
                json_response['errorcode'] = 'CHP1'
        else:
            json_response['errorcode'] = 'CHP2'
            json_response['status'] = 'f'
            err = dict()
            json_response['message'] = 'Form validation failed'
            for key, mes in chpassform.errors.iteritems():
                err[key]=mes
                print key,mes
            json_response['err_data'] = err
            
    return HttpResponse(json.dumps(json_response), content_type='application/json')
    
@csrf_exempt
def reset_password(request):
    """
        Error Codes:
            REP1 = Error Reset Password
            REP2 = Form is invalid

    """
    json_response = init_json()
    if request.POST or request.is_ajax():
        try:
            user = User.objects.get(username = request.POST['fusername'])
        except ObjectDoesNotExist:
            json_response['status'] = 'f'
            json_response['message'] = 'Invalid Username'
            json_response['errorcode'] = 'REP1'
        else:
            randpass = ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(6))
            user.set_password(randpass)
            authkey = UserAuthKey.objects.get(user = user)
            authkey.passreset = True
            user.save()
            authkey.save()
            sendPassword(user.username,user.email,randpass)

    return HttpResponse(json.dumps(json_response), content_type='application/json')

def sendPassword(username,email,password):
    subject = 'Carpoolindia password reset'
    body = 'Hi '+username+','+'\n'
    body += '  Your password has been reset to <b>' + password + '</b>'
    emailsend = EmailMessage(subject, body, settings.EMAIL_HOST_USER, (email,))
    emailsend.send()
    
@csrf_exempt
def get_vehicle_details(request):

    """
            Error Codes:
            VHI1 = Error Get Vehicle
    """

    json_response = init_json()
    if request.GET or request.is_ajax():
        try:
            vehicle = getVehicle(request.user)
            if vehicle:
                pool = Pool.objects.get(owner=request.user, poolvehic=vehicle)
                json_response['data']['car_number'] = vehicle.car_number
                json_response['data']['make'] = vehicle.make
                json_response['data']['color'] = vehicle.color
                json_response['data']['year'] = vehicle.year
                json_response['data']['fueleff'] = vehicle.fueleff
                json_response['data']['fuel'] = vehicle.fuel
                json_response['data']['pref_gender'] = pool.preferred_gender
                json_response['data']['totalmembers'] = pool.totalmembers
                json_response['data']['occupied'] = pool.occupied
                json_response['data']['freeseats'] = pool.freeseats
        except ObjectDoesNotExist:
            json_response['status'] = 'f'
            json_response['message'] = 'Invalid Username'
            json_response['errorcode'] = 'VHI1'


    return HttpResponse(json.dumps(json_response), content_type='application/json')


def testJson(request):
    json_response = {}
    json_response['users'] = getSyncData(User.objects.get(pk=1), False)
    json_response['test'] = [(1,2), (2,3)]
    return HttpResponse(json.dumps(json_response), content_type='application/json')


@csrf_exempt
def recom(request):
    """
        Error Codes:
            REP1 = Error Reset Password
            REP2 = Form is invalid

    """
    json_response = init_json()
    username = request.POST.get('username', '')
    profile = ''
    user = ''

    if username:
        user = getUser(username)

    if user:
        try:
            profile = Profile.objects.get(user=user)
        except ObjectDoesNotExist:
            profile = ''

    if profile:
        owner = profile.type
        startloc = profile.startlocation
        endloc = profile.endlocation
        finalData = getProfiles(owner, startloc, endloc)
    else:
        finalData = getProfiles(False, '', '')

    json_response['status'] = ''
    json_response['message'] = ''
    json_response['errorcode'] = ''
    json_response['data'] = finalData

    return HttpResponse(json.dumps(json_response), content_type='application/json')

@csrf_exempt
def search(request):
    """
        Error Codes:
            REP1 = Error Reset Password
            REP2 = Form is invalid

    """
    json_response = init_json()
    location = request.POST.get('location', '')
    profile = ''

    finalData = getProfiles('', location, location, True)

    json_response['status'] = ''
    json_response['message'] = ''
    json_response['errorcode'] = ''
    json_response['data'] = finalData

    return HttpResponse(json.dumps(json_response), content_type='application/json')


def getProfiles(owner, startlocation, endlocation, search=False):
    if owner == 'O':
        owner = True
        ownertype = 'M'
    else:
        owner = False
        ownertype = 'O'
    if search:
        profs= Profile.objects.filter(endlocation__icontains=endlocation)
        if not profs:
            profs= Profile.objects.filter(startlocation__icontains=endlocation)
    else:
        if owner:
            profs= Profile.objects.filter(endlocation__icontains=endlocation, type=ownertype)
        else:
            profs= Profile.objects.filter(endlocation__icontains=endlocation, type=ownertype)

        if not profs:
            if owner:
                profs= Profile.objects.filter(startlocation__icontains=startlocation, type=ownertype)
            else:
                profs= Profile.objects.filter(startlocation__icontains=startlocation, type=ownertype)


    finalData = []
    for prof in profs:
        towner = True if prof.type == 'O' else False
        temp = {}
        tvehicle = UserVehicle.objects.filter(user=prof.user, retired=False)
        if tvehicle:
            # TODO: Multiple vehicles needs to be worked later
            tvehicle = tvehicle[0]
        treviews = UserReviews.objects.filter(reviewee=prof.user)
        tpool = Pool.objects.filter(owner=prof.user, active=True)
        if tpool:
            tpool = tpool [0]
        temp['username'] = prof.user.username
        temp['ratings'] = prof.ratings_as_owner if towner else prof.ratings_as_poolseeker
        temp['reviews'] = treviews.count()
        temp['startlocation'] = prof.startlocation
        temp['endlocation'] = prof.endlocation
        temp['starttime'] = str(prof.start_work_time)
        temp['endtime'] = str(prof.end_work_time)
        temp['owner'] = prof.type
        if tvehicle:
            temp['car'] = tvehicle.make + ' ' + tvehicle.model
        else:
            temp['car'] = ''
        if tpool:
            temp['totalseats'] = tpool.totalmembers if towner else ''
            temp['freeseats'] = tpool.freeseats if towner else ''
        else:
            temp['totalseats'] = ''
            temp['freeseats'] = ''

        finalData.append(temp)

    return [seri for seri in finalData]


@csrf_exempt
def place(request, key=''):
    pac = PlaceAutoComplete()
    json_response = init_json()
    json_response['data'] = pac.autocompletes(key)
    return HttpResponse(json.dumps(json_response), content_type='application/json')    


@csrf_exempt
def getProfileDetails(request):
    json_response = init_json()
    json_response['data'] = dict()
    username = request.POST.get('proftoget', '')
    if username:
        user = User.objects.get(username=username)
        if user:
            profile = getProfile(user)
            if profile:
                json_response['data']['profile'] = profile.serialized()
                if profile.type == 'O':
                    vehicle = getVehicle(user)
                    pool = getPool(user)
                    if vehicle:
                        json_response['data']['vehicle'] = vehicle.serialized()
                        if pool:
                            json_response['data']['pool'] = pool.serialized()
                return HttpResponse(json.dumps(json_response), content_type='application/json')

    json_response['status'] = 'f'
    json_response['message'] = 'Unable to getProfile'
    json_response['errorcode'] = ''
    return HttpResponse(json.dumps(json_response), content_type='application/json')


@csrf_exempt
def joinunjoin(request):
    json_response = init_json()
    json_response['data'] = dict()
    joinname = request.POST.get('joinname', '')
    poolname = request.POST.get('poolname', '')
    poolid = request.POST.get('poolid', '')
    action = request.POST.get('action', '')

    if joinname:
        joinuser = getUser(joinname)
        joinprofile = getProfile(joinuser)
        pooluser = getUser(poolname)
        pool = Pool.objects.get(pk=poolid)
        if not pool or not  joinprofile or not pooluser:
            json_response['status'] = 'f'
            json_response['message'] = 'Unable to Join'
            json_response['errorcode'] = ''
            return HttpResponse(json.dumps(json_response), content_type='application/json')
        if pool:
            if action == 'J':
                pm = PoolMembers()
                pm.pool = pool
                pm.user = joinuser
                pm.startlocation = joinprofile.startlocation
                pm.endlocation = joinprofile.endlocation
                pm.endpos = joinprofile.endpos
                pm.startpos = joinprofile.startpos
                pm.active = False
                pm.start_date = now()
                pm.save()
            ju = JoinsUnjoins()
            ju.pool = pool
            ju.requested_by = joinuser
            ju.requested_to = pooluser
            ju.approved = False
            ju.request_type = action
            ju.msg = 'I would like to join your pool'
            ju.save()
            json_response['status'] = ''
            json_response['message'] = ''
            json_response['errorcode'] = ''
            json_response['data'] = poolname

            subject = 'Carpoolindia %s Request' % ('Join' if action=='J' else 'Unjoin')
            body = 'User %s has requested to %s the pool \n' % (joinname, 'Join' if action=='J' else 'Unjoin')
            emailsend = EmailMessage(subject, body, settings.EMAIL_HOST_USER, (joinuser.email,))
            emailsend.send()
            return HttpResponse(json.dumps(json_response), content_type='application/json')
    else:
        json_response['status'] = 'f'
        json_response['message'] = 'Unable to Join'
        json_response['errorcode'] = ''
        return HttpResponse(json.dumps(json_response), content_type='application/json')


@csrf_exempt
def syncRequests(request):
    json_response = init_json()
    json_response['data'] = dict()
    username = request.POST.get('user', '')
    data = list()
    if username:
        user = getUser(username)
        if user:
            ju = JoinsUnjoins.objects.filter(requested_to=user)
            data = [j.serialized() for j in ju]
            json_response['data'] = data
            json_response['status'] = ''
            json_response['message'] = ''
            json_response['errorcode'] = ''
            return HttpResponse(json.dumps(json_response), content_type='application/json')
    else:
        json_response['status'] = 'f'
        json_response['message'] = 'Unable to Sync Requests'
        json_response['errorcode'] = ''
        return HttpResponse(json.dumps(json_response), content_type='application/json')


@csrf_exempt
def syncRequests(request):
    json_response = init_json()
    json_response['data'] = dict()
    username = request.POST.get('user', '')
    data = list()
    if username:
        user = getUser(username)
        if user:
            ju = JoinsUnjoins.objects.filter(requested_to=user)
            data = [j.serialized() for j in ju]
            json_response['data'] = data
            json_response['status'] = ''
            json_response['message'] = ''
            json_response['errorcode'] = ''
            return HttpResponse(json.dumps(json_response), content_type='application/json')
    else:
        json_response['status'] = 'f'
        json_response['message'] = 'Unable to Sync Requests'
        json_response['errorcode'] = ''
        return HttpResponse(json.dumps(json_response), content_type='application/json')


@csrf_exempt
def approveRequest(request):
    json_response = init_json()
    json_response['data'] = dict()
    id = request.POST.get('poolid', '')
    username = request.POST.get('user', '')
    action = request.POST.get('action', '')
    requestedby= request.POST.get('requestedby', '')
    requestedbyuser = getUser(requestedby)
    data = {}
    if username:
        user = getUser(username)
        if user:
            ju = JoinsUnjoins.objects.get(pk=id, requested_to=user, requested_by=requestedbyuser)
            if action == 'A':
                ju.approved = True
                ju.save()
                pm = PoolMembers.objects.get(user=ju.requested_by, pool=ju.pool)
                pm.active = True
                sub = 'Your %s Request has been approved' % ('Join' if ju.request_type=='J' else 'Unjoin')
            else:
                sub = 'Your %s Request has been denied' % ('Join' if ju.request_type=='J' else 'Unjoin')

            subject = sub
            body = sub
            emailsend = EmailMessage(subject, body, settings.EMAIL_HOST_USER, (ju.requested_by.email,))
            emailsend.send()

            json_response['data'] = data
            json_response['status'] = ''
            json_response['message'] = ''
            json_response['errorcode'] = ''
            return HttpResponse(json.dumps(json_response), content_type='application/json')
    else:
        json_response['status'] = 'f'
        json_response['message'] = 'Unable to Sync Requests'
        json_response['errorcode'] = ''
        return HttpResponse(json.dumps(json_response), content_type='application/json')
