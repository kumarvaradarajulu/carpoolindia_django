from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import User
from django.core import serializers
from django.core.serializers.json import DjangoJSONEncoder
import json
import collections

from carpoolapp.models import Profile, UserVehicle, Pool


def getUser(username):
    try:
        user = User.objects.get(username=username)
        return user
    except ObjectDoesNotExist:
        return ''


def getProfile(user):
    try:
        profile = Profile.objects.get(user = user)
        return profile
    except ObjectDoesNotExist:
        return ''


def getVehicle(user):
    try:
        vehicle = UserVehicle.objects.get(user = user)
        return vehicle
    except ObjectDoesNotExist:
        return ''


def getPool(user):
    try:
        pool = Pool.objects.get(owner = user)
        return pool
    except ObjectDoesNotExist:
        return ''


def init_json():
    json_response = dict()
    json_response['data'] = dict()
    json_response['status'] = ''
    json_response['errorcode'] = ''
    json_response['message'] = ''
    json_response['trigger'] = list()

    return json_response
