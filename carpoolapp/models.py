from django.db import models
from django.contrib.auth.models import User
from django.utils.timezone import now
from sorl.thumbnail.fields import ImageField
from django.conf import settings
import re


def content_file_name(instance, filename):
    dirname = settings.THUMBNAIL_UPLOAD_TO_DIR
    filename = filename[-15:]
    return ''.join([dirname, re.sub(r'[,\.\ \+\-:]','',str(now())), '-', filename])


class UserVehicle(models.Model):
    user = models.ForeignKey(User)
    make = models.CharField(blank=False, max_length=100)
    model = models.CharField(blank=False, max_length=200)
    color = models.CharField(blank=True, null=True, max_length=30)
    year = models.IntegerField(blank=True, null=True, max_length=8)
    fueleff = models.IntegerField(blank=True, null=True, max_length=3)
    fuel = models.CharField(max_length=1, blank=False)
    date_time = models.DateTimeField(blank=False, default=now())
    retired = models.BooleanField(default=False)
    retired_date_time = models.DateTimeField(blank=True, null=True)

    def __unicode__(self):
        return '%s' % (self.user.username)

    def serialized(self):
        return {'make': self.make,
         'model': self.model,
         'year': self.year,
         'fueleff': self.fueleff,
         'fuel': self.fuel,
         'retired': 1 if self.retired else 0,
         'retired_date_time': str(self.retired_date_time),
         'date_time': str(self.date_time)}


class Pool(models.Model):
    owner = models.ForeignKey(User)
    startlocation = models.TextField(blank=False, max_length=200)
    startpos = models.TextField(blank=False, max_length=200)
    endlocation = models.TextField(blank=True, null=True, max_length=200)
    endpos = models.TextField(blank=True, null=True, max_length=200)
    distance = models.TextField(blank=True, null=True, max_length=100)
    journey_start_time = models.TimeField(blank=True, null=True)
    journey_end_time = models.TimeField(blank=True, null=True)
    active = models.BooleanField(blank=False, default=True)
    preferred_gender = models.CharField(blank=True, null=True, default='A', max_length=1)
    totalmembers = models.IntegerField(max_length=2, default=4)
    occupied = models.IntegerField(max_length=2, default=1)
    freeseats= models.IntegerField(max_length=2, default=3)
    poolvehic = models.ForeignKey(UserVehicle)
    date_time = models.DateTimeField(blank=False, default=now())

    def __unicode__(self):
        return '%s' % (self.owner.username)
    
    def serialized(self):
        if self.poolvehic:
            vehicle_id = self.poolvehic.id 
        else:
            vehicle_id = ''
        return {'id': self.id,
            'owner': self.owner.username,
         'startlocation': self.startlocation,
         'startpos': self.startpos,
         'endlocation': self.endlocation,
         'endpos': self.endpos,
         'journey_start_time': str(self.journey_start_time),
         'journey_end_time': str(self.journey_end_time),
         'preferred_gender': self.preferred_gender,
         'totalmembers': self.totalmembers,
         'occupied': self.occupied,
         'freeseats': self.freeseats,
         'poolvehic': vehicle_id,
         'date_time': str(self.date_time)}


class PoolMembers(models.Model):
    pool = models.ForeignKey(Pool, null=True)
    user = models.ForeignKey(User)
    startlocation = models.TextField(blank=False, max_length=200)
    startpos = models.TextField(blank=False, max_length=200)
    endlocation = models.TextField(blank=True, null=True, max_length=200)
    endpos = models.TextField(blank=True, null=True, max_length=200)
    distancefromowner = models.DecimalField(blank=True, null=True, max_digits=3, decimal_places=2)
    active = models.BooleanField(default=True)
    start_date = models.DateTimeField(blank=True, null=True)
    end_date = models.DateTimeField(blank=True, null=True)
    date_time = models.DateTimeField(default=now())

    def __unicode__(self):
        return '%s %s' % (self.pool.id, self.user.username)

    def serialized(self):
        if self.pool:
            pool_id = self.pool.id
        else:
            pool_id = ''
        return {'pool': pool_id,
                'user': self.user.username,
                'startlocation': self.startlocation,
                'startpos': self.startpos,
                'endlocation': self.endlocation,
                'endpos': self.endpos,
                'distancefromowner': self.distancefromowner,
                'date_time': str(self.date_time)}

class Profile(models.Model):
    user = models.ForeignKey(User)
    type = models.CharField(max_length=1, default='M', blank=False) # M for Member, O for Owner
    type_change_date = models.DateTimeField(blank=True, null=True)
    start_work_time = models.TimeField(blank=True, null=True)
    end_work_time = models.TimeField(blank=True, null=True)
    gender = models.CharField(max_length=1, blank=True, null=True)
    pool = models.ForeignKey(Pool, blank=True, null=True)
    ratings_as_owner = models.DecimalField(blank=True, null=True, max_digits=2, decimal_places=1)
    ratings_as_poolseeker = models.DecimalField(blank=True, null=True, max_digits=2, decimal_places=1)
    phone = models.CharField(max_length=10, default='', blank=True)
    startlocation = models.TextField(blank=False, max_length=200)
    startpos = models.TextField(blank=False, max_length=100)
    endlocation = models.TextField(blank=True, null=True, max_length=200)
    endpos = models.TextField(blank=True, null=True, max_length=100)
    numofabuses = models.IntegerField(default=0)
    date_time = models.DateTimeField(blank=False, default=now())
    preferred_gender = models.CharField(max_length=1, blank=True, null=True)
    prefernces = models.TextField(max_length=1000, blank=True, null=True)
    avatar = ImageField(blank=True, null=True, upload_to=content_file_name)

    def __unicode__(self):
        return '%s' % (self.user.username)

    def serialized(self):
        if self.pool:
            pool_id = self.pool.id
        else:
            pool_id = ''
        return{'user': self.user.username,
               'type': self.type,
               'type_change_date': str(self.type_change_date),
               'start_work_time': str(self.start_work_time),
               'end_work_time': str(self.end_work_time),
               'gender': self.gender,
               'pool': pool_id,
               'date_time': str(self.date_time),
               'phone': self.phone,
               'startlocation': self.startlocation,
               'endlocation': self.endlocation,
               'startpos': self.startpos,
               'preferred_gender': self.preferred_gender,
               'prefernces': self.prefernces,
               'avatar': self.avatar.name,
               'endpos': self.endpos
        }


class UserRoute(models.Model):
    user = models.ForeignKey(User)
    startlocation = models.TextField(blank=False, max_length=200)
    startpos = models.TextField(blank=False, max_length=100)
    endlocation = models.TextField(blank=True, null=True, max_length=200)
    endpos = models.TextField(blank=True, null=True, max_length=100)
    distance = models.TextField(blank=True, null=True, max_length=100)
    date_time = models.DateTimeField(blank=False, default=now())


class PoolCalendar(models.Model):
    pool = models.ForeignKey(Pool, blank=False)
    month = models.IntegerField(blank=False, max_length=1)
    daysoff = models.CharField(max_length=500, blank=True)
    dayson = models.CharField(max_length=500, blank=True) # Days coming on holidays
    date_time = models.DateTimeField(blank=False, default=now())

    def serialized(self):
        if self.pool:
            pool_id = self.pool.id
        else:
            pool_id = ''
        return {'pool': pool_id,
                'month': self.month,
                'daysoff': self.daysoff,
                'dayson': self.dayson,
                'date_time': str(self.date_time)}


class PoolMembersCalendar(models.Model):
    user = models.ForeignKey(User)
    pool = models.ForeignKey(Pool, blank=False)
    month = models.IntegerField(blank=False, max_length=1)
    daysoff = models.CharField(max_length=500, blank=True, null=True)
    dayson = models.CharField(max_length=500, blank=True, null=True) # Days coming on holidays
    date_time = models.DateTimeField(blank=False, default=now())

    def serialized(self):
        if self.pool:
            pool_id = self.pool.id
        else:
            pool_id = ''
        return {'user': self.user.username,
                'pool': pool_id,
                'month': self.month,
                'daysoff': self.daysoff,
                'dayson': self.dayson,
                'date_time': str(self.date_time)}


class UserReviews(models.Model):
    reviewer = models.ForeignKey(User, related_name='reviewer_name')
    reviewee = models.ForeignKey(User, related_name='reviewee_name')
    reviewer_type = models.CharField(max_length=1)
    reviewee_type = models.CharField(max_length=1)
    review = models.TextField(max_length=1500)
    rating = models.IntegerField(max_length=1, default=5)
    date_time = models.DateTimeField(blank=False, default=now())

    def serialized(self):
        return {'reviewer': self.reviewer.username,
                'reviewee': self.reviewee.username,
                'reviewer_type': self.reviewer_type,
                'reviewee_type': self.reviewee_type,
                'review': self.review,
                'rating': self.rating,
                'date_time': str(self.date_time)}


class Buzz(models.Model):
    pool = models.ForeignKey(Pool)
    buzzed_by = models.ForeignKey(User)
    buzzed_to = models.ForeignKey(User, related_name='buzzed_to')
    buzz_msg = models.CharField(max_length=400)
    buzz_importance = models.IntegerField(max_length=1, default=3) # Scale of 1-5 1 being lowest and 5 being highest
    date_time = models.DateTimeField(blank=False, default=now())

    def serialized(self):
        if self.pool:
            pool_id = self.pool.id
        else:
            pool_id = ''
        return {'buzzed_by': self.buzzed_by.username,
                'buzzed_to': self.buzzed_to.username,
                'pool': pool_id,
                'buzz_msg': self.buzz_msg,
                'buzz_importance': self.buzz_importance,
                'date_time': str(self.date_time)}


class JoinsUnjoins(models.Model):
    requested_by = models.ForeignKey(User, related_name='requested_by')
    requested_to = models.ForeignKey(User, related_name='requested_to')
    approved = models.BooleanField(default=False)
    approval_date_time = models.DateTimeField(blank=True, null=True)
    request_type = models.CharField(max_length=1) # J for Join U for Unjoin
    msg = models.TextField(max_length=1000, blank=True, null=True)
    date_time = models.DateTimeField(blank=False, default=now())
    pool = models.ForeignKey(Pool)

    def __unicode__(self):
        return '%s %s' % (self.requested_by.username, self.requested_to.username)

    def serialized(self):
        return {'id': self.id,
            'requested_by': self.requested_by.username,
                'requested_to': self.requested_to.username,
                'approved': self.approved,
                'request_type': self.request_type,
                'msg': self.msg,
                'date_time': str(self.date_time)
        }



class UserAuthKey(models.Model):
    user = models.ForeignKey(User)
    authkey = models.CharField(max_length=500) # This is encrypted key
    devid = models.CharField(max_length=500) # This is encrypted key
    devkey = models.CharField(max_length=500) # This is encrypted key
    activated = models.BooleanField(default=False, blank=False)
    passreset = models.BooleanField(default=False, blank=False)
    date_time = models.DateTimeField(blank=False, default=now())


    def serialized(self):
        if self.pool:
            pool_id = self.pool.id
        else:
            pool_id = ''
        return {'user': self.user.username,
                'pool': pool_id,
                'sky': self.sky,
                'did': self.did,
                'date_time': str(self.date_time)}

class UserActivtion(models.Model):
    user = models.ForeignKey(User)
    authkey = models.ForeignKey(UserAuthKey)
    acvtkey = models.CharField(max_length=500)
    date_time = models.DateTimeField(blank=False, default=now())


class UserAbuse(models.Model):
    user = models.ForeignKey(User, related_name='user_abused')
    reportedby = models.ForeignKey(User, related_name='abuse_reportedby')
    date_time = models.DateTimeField(blank=False, default=now())

class Preferences(models.Model):
    name1 = models.CharField(max_length=50, blank=True, null=True)
    value1 = models.CharField(max_length=50, blank=True, null=True)
    name2 = models.CharField(max_length=50, blank=True, null=True)
    value2 = models.CharField(max_length=50, blank=True, null=True)
    name3 = models.CharField(max_length=50, blank=True, null=True)
    value3 = models.CharField(max_length=50, blank=True, null=True)
    name4 = models.CharField(max_length=50, blank=True, null=True)
    value4 = models.CharField(max_length=50, blank=True, null=True)
    name5 = models.CharField(max_length=50, blank=True, null=True)
    value5 = models.CharField(max_length=50, blank=True, null=True)
    name6 = models.CharField(max_length=50, blank=True, null=True)
    value6 = models.CharField(max_length=50, blank=True, null=True)
    name7 = models.CharField(max_length=50, blank=True, null=True)
    value7 = models.CharField(max_length=50, blank=True, null=True)
    name8 = models.CharField(max_length=50, blank=True, null=True)
    value8 = models.CharField(max_length=50, blank=True, null=True)
    name9 = models.CharField(max_length=50, blank=True, null=True)
    value9 = models.CharField(max_length=50, blank=True, null=True)
    name10 = models.CharField(max_length=50, blank=True, null=True)
    value10 = models.CharField(max_length=50, blank=True, null=True)

    def serialized(self):
        return{'name1': self.name1,
               'value1': self.value1,
               'name2': self.name2,
               'value2': self.value2,
               'name3': self.name3,
               'value3': self.value3,
               'name4': self.name4,
               'value4': self.value4,
               'name5': self.name5,
               'value5': self.value5,
               'name6': self.name6,
               'value6': self.value6,
               'name7': self.name7,
               'value7': self.value7,
               'name8': self.name8,
               'value8': self.value8,
               'name9': self.name9,
               'value9': self.value9,
               'name10': self.name10,
               'value10': self.value10
        }
