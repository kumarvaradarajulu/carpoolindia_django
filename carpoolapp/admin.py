from django.contrib import admin
from carpoolapp.models import Profile, PoolMembers, PoolCalendar, Pool, UserAuthKey, UserVehicle, UserActivtion, UserReviews, UserRoute, JoinsUnjoins

admin.site.register(Profile)
admin.site.register(PoolMembers)
admin.site.register(PoolCalendar)
admin.site.register(Pool)
admin.site.register(UserAuthKey)
admin.site.register(UserVehicle)
admin.site.register(UserActivtion)
admin.site.register(UserReviews)
admin.site.register(UserRoute)
admin.site.register(JoinsUnjoins)